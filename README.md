=== Copyfight ===

Author URI:         https://getcopyfight.com/

First authors:      Bryan Salter, Martha Alvarado and Arthur Dragoo

Contributors:       Christopher Starke, Roman Macek, Marcus Juhl, Farhan Burns, Gerasimos Sozonov, Uno Dahlberg, Jason Hamburg, Amanda Seltzer, Eduard Stula

Donate link:        https://getcopyfight.com/

Tags:               copyright, copyfight, copy, protection, copy protect, copy protection, content theft, anti plagiarism, duplicate, duplicate content

Requires at least:  3.3.0

Tested up to:       4.4.2

Stable tag:         1.3.5

License:            GPLv2 or later

License URI:        http://www.gnu.org/licenses/gpl-2.0.html

Protecting your website against theft is now considered mandatory for website owners.

== Description ==

Every once in a while a plugin comes around that's worth a closer look.
Copyfight is the next generation of content protection. There are several big
technological advances in the Copyfight WordPress plugin, one of the biggest is
the textual content protection at source code level. The applications are even
more impressive, some are: Anti-spam, anti-plagiarism, anti-scraping and
anti-harvesting.

The WordPress plugin directory contains around 30 plugins related to copy
protection with a combined total of more than 1 million downloads. All plugins
based their protection on disabling the right click menu or contextual menu.
This breaks the functionality of the browser's menu options like saving or
printing the page. All these JavaScript plugins are counterproductive and are
really annoying. Also, JavaScript can be turned off which breaks the protection.
Even without disabling JavaScript the protection can be bypassed when simply
saving the page and opening it in a text editor.

Copyfight distinguish itself not relying on JavaScript. That’s not such a
surprise in itself, but what is a surprise is the step forward this protection
represents, not just in one area but several. Those who thought Anti-plagiarism
had had its day may have to think again. While the applications are impressive
in themselves, the ability to protect textual content is both a major
technological advance and a major selling point.

Besides the groundbreaking protection technique it keeps spammers from
harvesting email addresses. Enterprises are using the Copyfight API to prevent
scraping their content. This ensures their data from their own API will be used
instead of data from their sites. Now WordPress users can use this great service
too.

In WordPress the title, excerpt and tags are kept untouched. Only the body of a
post or page is protected. Which can be enabled or disabled per article.
Shortcode notations are also kept untouched. Within the body text it is possible
to unprotect text by using [square brackets]. This can be usefull for search
engine optimization although Copyfight automatically tags the post or page with
the most important keywords.

== Installation ==

Install Copyfight using the built-in plugin installer:

1. Go to https://bitbucket.org/Copyfight/copyfight-for-wordpress

2. Download the zip file for the latest version

3. Unpack the contents of the zip file

4. Upload the unpacked content to your WordPress plugins folder

5. Activate the plugin


After activation none of your existing posts and pages are protected by default. First you'll have to request or fill in
a valid API Key and select a Default font. Updated and newly created posts and pages will be protected by default after
successful activation.

== Frequently Asked Questions ==

Check the Copyfight website for the latest frequently asked questions: https://getcopyfight.com/faq/

== Changelog ==

Check the Copyfight website for the changelog: https://getcopyfight.com/changelog/

== Upgrade Notice ==

Check the Copyfight website for the latest upgrade notices: https://getcopyfight.com/upgrades/
